RHL photos, static copy of the website
======================================

This is a static copy of the website of the photos of Rencontres
Hivernales du Libre.



Build
-----

With Podman:
```sh
podman build -t photos.hivernal.es:latest .
```


With Docker:
```sh
docker build -t photos.hivernal.es:latest .
```


RUN
---

With Podman:
```sh
podman run photos.hivernal.es:latest
```

With Docker:
```sh
docker run photos.hivernal.es:latest
```


Author
------

Sébastien Gendre <seb@k-7.ch>
